# -*- coding: utf-8 -*-
"""
Given an array of integers, return indices of the two numbers 
such that they add up to the target value.

You may assume that each input would have exactly one 
solution, and you will not use the same element twice.

Example:

Given nums = [2, 8, 11, 15], target = 10,

Because nums[0] + nums[1] = 2 + 8 = 10,
return [0, 1].
"""

